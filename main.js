/**
 * Created by 911 on 27/6/2016.
 */

//Load Express module
var express = require("express");
//Create an instance of Express application
var app = express();

//Process command line arguments
// -p port
// -d dir

var port = 3000;
var staticDir = __dirname + "/public";

var version = 1;
var i=2;
while (i < process.argv.length) {
    console.info ("in the loop");
    switch (process.argv[i]) {
   
        case "-v":
            i = i + 1;
            console.info("Version :%d", version);
            break;
        case "-p":
            i = i + 1;
            port = process.argv[i];
            break;
        case "-d":
            i = i + 1;
            staticDir = process.argv[i];
            break;
        default:
    }
    i += 1;
}
// Use public directory for static files
app.use(
    express.static(staticDir));

// Set a port
app.set("port", port);

//Start the server on port
app.listen (
    app.get("port"),
    function () {
        console.info("%s started on port %d", process.argv[1], app.get("port"));
        console.info("\tstatic directory: %s", staticDir);
    }
);
